STRV Weather APP
============
This is the test project for STRV


Dependencies
============

* [Android Support Library](http://developer.android.com/tools/support-library/index.html)
* [AppCompat](https://developer.android.com/reference/android/support/v7/appcompat/package-summary.html)
* [Google Play Services](http://developer.android.com/google/play-services/index.html)
* [GSON](http://code.google.com/p/google-gson/)
* [OkHttp](https://github.com/square/okhttp)
* [Retrofit](https://github.com/square/retrofit)
* [Glide](https://github.com/bumptech/glide)
* [Butterknife](https://github.com/JakeWharton/butterknife)
* [Timber](https://github.com/JakeWharton/timber)

Developed by
============

[Federico Toscano](http://dolen.org)


License
=======

    Copyright © 2015 Federico Toscano
    This work is free. You can redistribute it and/or modify it under the
    terms of the Do What The Fuck You Want To Public License, Version 2,
    as published by Sam Hocevar. See the COPYING file for more details.
