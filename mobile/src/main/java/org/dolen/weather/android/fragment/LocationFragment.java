package org.dolen.weather.android.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.dolen.weather.android.activity.MainActivity;

import timber.log.Timber;

public class LocationFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    
    MainActivity activity;
    protected GoogleApiClient mGoogleApiClient;
    private LatLng mLastLocation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }


    @Override
    public void onPause() {
        mGoogleApiClient.disconnect();
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("loc", mLastLocation);
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location newLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LatLng newLatLng = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

        // requesting weather if location has changed
        if(!newLatLng.equals(mLastLocation)) {
            mLastLocation = newLatLng;
            Timber.d("new location: " + mLastLocation.toString());
            onLocationChanged();
        } else {
            Timber.d("Location hasn't changed");
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        // connection error to play services
        Timber.e("Play services error");
        activity.showNetworkErrorFragment();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // connection error to play services
        Timber.e("Play services error");
        activity.showNetworkErrorFragment();
    }


    public void onLocationChanged() {}


    public LatLng getLastLocation() {
        return mLastLocation;
    }
    
}
