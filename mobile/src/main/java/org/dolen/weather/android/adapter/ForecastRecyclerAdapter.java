package org.dolen.weather.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.dolen.weather.android.R;
import org.dolen.weather.android.WeatherConfig;
import org.dolen.weather.android.api.model.TodayWeatherWrapper;
import org.dolen.weather.android.utility.UnitsUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ForecastRecyclerAdapter extends
        RecyclerView.Adapter<ForecastRecyclerAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<TodayWeatherWrapper> mItems;
    private String mTemperatureUnit;


    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        @InjectView(R.id.forecast_item_description)
        public TextView forecast_item_description;
        @InjectView(R.id.forecast_item_degrees)
        public TextView forecast_item_degrees;
        @InjectView(R.id.forecast_item_icon)
        public ImageView forecast_item_icon;

        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

    }


    public ForecastRecyclerAdapter(Context context) {
        super();
        mContext = context;
    }


    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.forecast_list_item, parent, false);
        return new SimpleViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {

        TodayWeatherWrapper todayWeatherWrapper = mItems.get(position);

        // getting the literal day name
        Date date = new Date(1000L * todayWeatherWrapper.getDt());
        DateFormat formatter = new SimpleDateFormat("EEEE", Locale.US);
        String day_str = formatter.format(date);
        String weather_desc = todayWeatherWrapper.getWeather().get(0).getDescription() +
                " on " + day_str;
        // first character to uppercase
        weather_desc = weather_desc.substring(0,1).toUpperCase() + weather_desc.substring(1);
        holder.forecast_item_description.setText(weather_desc);

        String degrees = String.valueOf(
                todayWeatherWrapper.getMain().getTemp(mContext, mTemperatureUnit)) +
                UnitsUtils.getTempUnitSymbol(mContext, mTemperatureUnit);

        holder.forecast_item_degrees.setText(degrees);

        // loading the weather condition icon
        String iconUrl = WeatherConfig.OPENWEATHER_ICONS_URL +
                todayWeatherWrapper.getWeather().get(0).getIcon() + "." +
                WeatherConfig.OPENWEATHER_ICONS_EXT;

        Glide.with(mContext)
                .load(iconUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.forecast_item_icon);
    }


    @Override
    public int getItemCount() {
        return (mItems!=null)?mItems.size():0;
    }


    public void setTemperatureUnit(String temperatureUnit) {
        mTemperatureUnit = temperatureUnit;
    }


    public void setItems(List<TodayWeatherWrapper> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}
