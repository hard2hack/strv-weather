package org.dolen.weather.android.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.dolen.weather.android.R;
import org.dolen.weather.android.fragment.ForecastFragment;
import org.dolen.weather.android.fragment.NetworkErrorFragment;
import org.dolen.weather.android.fragment.TodayFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity  implements
        NavigationView.OnNavigationItemSelectedListener {

    @InjectView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    @InjectView(R.id.nav_view)
    protected NavigationView mNavView;
    @InjectView(R.id.toolbar)
    protected Toolbar mToolbar;

    private ActionBarDrawerToggle mNavToggle;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setup();
        Timber.e("onCreate: " +(savedInstanceState == null));
        if (savedInstanceState != null) {
            currentFragment = getFragmentManager().getFragment(savedInstanceState, "current_fragment");
            if (currentFragment != null) {
                showFragment(currentFragment);
            } else {
                showTodayFragment();
            }
        } else {
            showTodayFragment();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getFragmentManager().putFragment(outState, "current_fragment", currentFragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                showSettingsActivity();
                return true;
            case R.id.action_about:
                showAboutDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        // TODO: maybe a little delay to allow the user to see the transition
        switch (menuItem.getItemId()) {
            case R.id.nav_item_today:
                showTodayFragment();
                break;
            case R.id.nav_item_forecast:
                showForecastFragment();
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void setup() {
        setupDrawer();
        if (!isDeviceConnected()) {
            showNetworkErrorFragment();
        }
    }


    public boolean isDeviceConnected() {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }


    public void setupDrawer() {
        setupToolbar();
        mNavToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mNavToggle);
        mNavView.setNavigationItemSelectedListener(this);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mNavToggle.syncState();
            }
        });
    }


    public void setupToolbar() {
        setSupportActionBar(mToolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            // enable ActionBar app icon to behave as action to toggle nav drawer
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle(R.string.app_name);
        }
    }


    public void showFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName());

        if(addToBackStack) {
            ft.addToBackStack(fragment.getClass().getSimpleName());
        }
        ft.commit();
        Timber.e("fragment: " + fragment.getClass().getSimpleName());
        currentFragment = fragment;
    }


    public void showFragment(Fragment fragment) {
        showFragment(fragment, true);
    }


    public void showTodayFragment() {
        showFragment(TodayFragment.newInstance());
    }


    public void showForecastFragment() {
        showFragment(ForecastFragment.newInstance());
    }
    
    
    public void showNetworkErrorFragment() {
        showFragment(NetworkErrorFragment.newInstance());
    }


    public void showSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        // place here your bundle
        startActivity(intent);
    }


    public void showAboutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.about_dialog_message)
                .setPositiveButton(R.string.about_dialog_positive_button,
                        new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                });
        alertDialog.create().show();

    }

}
