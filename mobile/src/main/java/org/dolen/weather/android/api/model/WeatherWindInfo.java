package org.dolen.weather.android.api.model;

import android.content.Context;

import org.dolen.weather.android.R;
import org.dolen.weather.android.utility.UnitsUtils;

import java.io.Serializable;

public class WeatherWindInfo implements Serializable {

    private double speed;
    private double deg;


    public double getSpeed() {
        return speed;
    }


    public int getSpeed(Context context, String unit) {
        // default in kelvin
        if (unit.equals(context.getString(R.string.weather_lenght_unit_imperial_value))) {
            return UnitsUtils.mPerSecToMilesPerHour(getSpeed());
        }

        return (int) Math.round(getSpeed());
    }


    public void setSpeed(double speed) {
        this.speed = speed;
    }


    public double getDeg() {
        return deg;
    }


    public void setDeg(double deg) {
        this.deg = deg;
    }
}
