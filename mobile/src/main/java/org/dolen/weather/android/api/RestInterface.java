package org.dolen.weather.android.api;

import org.dolen.weather.android.api.model.ForecastWeatherWrapper;
import org.dolen.weather.android.api.model.TodayWeatherWrapper;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RestInterface {

    // OpenWeather API call for the today page
    @GET("/weather/")
    void getTodayWeather(@Query("lat") double lat, @Query("lon") double lon,
                         Callback<TodayWeatherWrapper> callback);


    // OpenWeather API call for the forecast page
    @GET("/forecast/")
    void getWeeklyForecast(@Query("lat") double lat, @Query("lon") double lon,
                         Callback<ForecastWeatherWrapper> callback);
}
