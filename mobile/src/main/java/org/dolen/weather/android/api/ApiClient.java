package org.dolen.weather.android.api;

import com.squareup.okhttp.OkHttpClient;

import org.dolen.weather.android.WeatherConfig;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class ApiClient {
    private static RestInterface restInterface;


    public static RestInterface getRestInterface() {
        if (restInterface == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(WeatherConfig.OPENWEATHER_API_ENDPOINT)
                    .setClient(new OkClient(new OkHttpClient()))
//                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();

            restInterface = restAdapter.create(RestInterface.class);
        }

        return restInterface;
    }
}