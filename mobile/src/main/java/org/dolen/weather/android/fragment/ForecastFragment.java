package org.dolen.weather.android.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.dolen.weather.android.R;
import org.dolen.weather.android.adapter.ForecastRecyclerAdapter;
import org.dolen.weather.android.api.ApiClient;
import org.dolen.weather.android.api.model.ForecastWeatherWrapper;
import org.dolen.weather.android.api.model.TodayWeatherWrapper;
import org.dolen.weather.android.layout.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

public class ForecastFragment extends LocationFragment {

    @InjectView(R.id.forecast_list)
    RecyclerView mForecastList;
    @InjectView(R.id.forecast_pb_loading)
    ProgressBar mProgressLoading;

    RecyclerView.LayoutManager mLayoutManager;
    ForecastRecyclerAdapter forecastRecyclerAdapter;
    ForecastWeatherWrapper mForecastWeatherWrapper;

    String temperatureUnit;


    public static ForecastFragment newInstance() {
        ForecastFragment fragment = new ForecastFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    public ForecastFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // get your arguments here
        }

        if(activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setTitle(R.string.title_forecast);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        ButterKnife.inject(this, view);

        mForecastList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mForecastList.setLayoutManager(mLayoutManager);
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        mForecastList.addItemDecoration(itemDecoration);
        forecastRecyclerAdapter = new ForecastRecyclerAdapter(getActivity());
        mForecastList.setAdapter(forecastRecyclerAdapter);

        if (getLastLocation() != null) {
            Timber.e("last_location not null");
            renderForecast();
        } else {
            Timber.e("last_location null");
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        readPref();
        if (getLastLocation() != null) {
            renderForecast();
        }
    }


    @Override
    public void onLocationChanged() {
        super.onLocationChanged();
        loadForecast();
    }


    public void readPref() {
        // get shared preferences
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activity);
        temperatureUnit = pref.getString(getString(R.string.weather_pref_temperature_key),
                getString(R.string.weather_pref_temperature_units_default_value));
    }


    public void loadForecast(){
        hideUI();
        ApiClient.getRestInterface()
                .getWeeklyForecast(getLastLocation().latitude, getLastLocation().longitude,
                        new Callback<ForecastWeatherWrapper>() {
                            @Override
                            public void success(ForecastWeatherWrapper forecastWeatherWrapper,
                                                Response response) {
                                Timber.d("API call success");
                                mForecastWeatherWrapper = forecastWeatherWrapper;
                                renderForecast();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Timber.e("API call failed.");
                                if (error.getResponse() != null) {
                                    Timber.e("Status: " + error.getResponse().getStatus());
                                    Timber.e("Reason: " + error.getResponse().getReason());
                                } else {
                                    Timber.e("Null response ");
                                }
                                activity.showNetworkErrorFragment();
                            }
                        });
    }


    public void showUI() {
        mForecastList.setVisibility(View.VISIBLE);
        mProgressLoading.setVisibility(View.INVISIBLE);
    }


    public void hideUI() {
        mForecastList.setVisibility(View.INVISIBLE);
        mProgressLoading.setVisibility(View.VISIBLE);
    }


    public void renderForecast() {
        showUI();

        // passing to the adapter only the weather for a certain time during the day
        ArrayList<TodayWeatherWrapper> forecastList = new ArrayList<>();
        for (TodayWeatherWrapper tww : mForecastWeatherWrapper.getList()) {
            if (tww.getDt_txt().contains(
                    // TODO: consider timezone
                    activity.getString(R.string.weather_forecast_daily_time))){
                forecastList.add(tww);
            }
        }
        forecastRecyclerAdapter.setItems(forecastList);
        forecastRecyclerAdapter.setTemperatureUnit(temperatureUnit);
    }

}
