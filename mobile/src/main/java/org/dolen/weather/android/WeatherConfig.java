package org.dolen.weather.android;

public class WeatherConfig {
    public static final String OPENWEATHER_API_ENDPOINT = "http://api.openweathermap.org/data/2.5";
    public static final String OPENWEATHER_ICONS_URL = "http://openweathermap.org/img/w/";
    public static final String OPENWEATHER_ICONS_EXT = "png";
}
