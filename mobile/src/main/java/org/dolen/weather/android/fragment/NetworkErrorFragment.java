package org.dolen.weather.android.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.dolen.weather.android.R;
import org.dolen.weather.android.activity.MainActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NetworkErrorFragment extends Fragment {

    MainActivity activity;

    @InjectView(R.id.btn_reconnect)
    Button mBtnReconnect;


    public static NetworkErrorFragment newInstance() {
        NetworkErrorFragment fragment = new NetworkErrorFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    public NetworkErrorFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // get your arguments here
        }

        if(activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setTitle(R.string.title_network_error);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_error, container, false);
        ButterKnife.inject(this, view);

        mBtnReconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setup();
            }
        });

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

}
