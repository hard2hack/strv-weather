package org.dolen.weather.android.utility;

import android.content.Context;

import org.dolen.weather.android.R;

public class UnitsUtils {

    public static String deg2direction(double degrees) {
        int index = (int) Math.round(degrees/22.5);
        String[] dirs = {"N","NNE","NE","ENE","E","ESE", "SE",
                "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"};

        return dirs[(index % dirs.length)];
    }


    public static int kelvinToCelsius(double temperature) {
        // http://www.rapidtables.com/convert/temperature/how-kelvin-to-celsius.htm
        return (int) Math.round(temperature - 273.15);
    }


    public static int kelvinToFahrenheit(double temperature) {
        // http://www.rapidtables.com/convert/temperature/how-kelvin-to-fahrenheit.htm
        return (int) Math.round(temperature*9/5 - 459.67);
    }


    public static int mPerSecToMilesPerHour(double speed) {
        // http://calculator-converter.com/converter_ms_to_mph_calculator.php
        return (int) Math.round(speed*2.23694 * 100) / 100;
    }


    public static String getTempUnitSymbol(Context context, String unit) {
        String temperatureUnitSymbol = "";
        if (unit
                .equals(context.getString(R.string.weather_temperature_unit_celsius_value))) {
            temperatureUnitSymbol =
                    context.getString(R.string.weather_temperature_unit_celsius_symbol);
        } else if (unit
                .equals(context.getString(R.string.weather_temperature_unit_fahrenheit_value))) {
            temperatureUnitSymbol =
                    context.getString(R.string.weather_temperature_unit_fahrenheit_symbol);
        } else if (unit
                .equals(context.getString(R.string.weather_temperature_unit_kelvin_value))) {
            temperatureUnitSymbol =
                    context.getString(R.string.weather_temperature_unit_kelvin_symbol);
        }

        return temperatureUnitSymbol;
    }


    public static String getWindSpeedUnitSymbol(Context context, String unit) {
        String speedUnitSymbol = "";
        if (unit
                .equals(context.getString(R.string.weather_lenght_unit_metric_value))) {
            speedUnitSymbol =
                    context.getString(R.string.weather_wind_speed_metric_unit_symbol);
        } else if (unit
                .equals(context.getString(R.string.weather_lenght_unit_imperial_value))) {
            speedUnitSymbol =
                    context.getString(R.string.weather_wind_speed_imperial_unit_symbol);
        }

        return speedUnitSymbol;
    }
}
