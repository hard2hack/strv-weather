package org.dolen.weather.android.api.model;

import java.io.Serializable;

public class WeatherSummary implements Serializable {

    private String main;
    private String description;
    private String icon;


    public String getMain() {
        return main;
    }


    public void setMain(String main) {
        this.main = main;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getIcon() {
        return icon;
    }


    public void setIcon(String icon) {
        this.icon = icon;
    }
}
