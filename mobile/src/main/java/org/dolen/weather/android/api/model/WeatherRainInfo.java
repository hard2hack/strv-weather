package org.dolen.weather.android.api.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WeatherRainInfo implements Serializable {

    @SerializedName("3h")
    private double precipitation;


    public double getPrecipitation() {
        return precipitation;
    }


    public void setPrecipitation(double precipitation) {
        this.precipitation = precipitation;
    }
}
