package org.dolen.weather.android.api.model;

import android.content.Context;

import org.dolen.weather.android.R;
import org.dolen.weather.android.utility.UnitsUtils;

import java.io.Serializable;

public class WeatherMainInfo implements Serializable {

    private double temp;
    private double pressure;
    private double humidity;
    private double temp_min;
    private double temp_max;
    private double sea_level;
    private double grnd_level;


    public double getTemp() {
        return temp;
    }


    public int getTemp(Context context, String unit) {
        // default in kelvin
        if (unit.equals(context.getString(R.string.weather_temperature_unit_celsius_value))) {
            return UnitsUtils.kelvinToCelsius(getTemp());
        }
        if (unit.equals(context.getString(R.string.weather_temperature_unit_fahrenheit_value))) {
            return UnitsUtils.kelvinToFahrenheit(getTemp());
        }

        return (int) Math.round(getTemp());
    }


    public void setTemp(double temp) {
        this.temp = temp;
    }


    public int getPressure() {
        return (int) Math.round(pressure);
    }


    public void setPressure(double pressure) {
        this.pressure = pressure;
    }


    public int getHumidity() {
        return (int) Math.round(humidity);
    }


    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }


    public double getTemp_min() {
        return temp_min;
    }


    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }


    public double getTemp_max() {
        return temp_max;
    }


    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }


    public double getSea_level() {
        return sea_level;
    }


    public void setSea_level(double sea_level) {
        this.sea_level = sea_level;
    }


    public double getGrnd_level() {
        return grnd_level;
    }


    public void setGrnd_level(double grnd_level) {
        this.grnd_level = grnd_level;
    }
}
