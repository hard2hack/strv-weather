package org.dolen.weather.android.fragment;

import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.dolen.weather.android.R;
import org.dolen.weather.android.WeatherConfig;
import org.dolen.weather.android.api.ApiClient;
import org.dolen.weather.android.api.model.TodayWeatherWrapper;
import org.dolen.weather.android.utility.UnitsUtils;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

public class TodayFragment extends LocationFragment {

    @InjectView(R.id.img_today_city_pic)
    ImageView mImgTodayCityPic;
    @InjectView(R.id.today_pb_loading)
    ProgressBar mProgressLoading;
    @InjectView(R.id.today_summary)
    RelativeLayout mRelLayoutTodaySummary;
    @InjectView(R.id.today_stats)
    RelativeLayout mRelLayoutTodayStats;
    @InjectView(R.id.txt_today_city_name)
    TextView mTxtTodayCityName;
    @InjectView(R.id.txt_today_weather_condition)
    TextView mTxtTodayWeatherCondition;
    @InjectView(R.id.img_today_weather_condition)
    ImageView mImgTodayWeatherCondition;
    @InjectView(R.id.txt_today_temperature)
    TextView mTxtTodayTemperature;
    @InjectView(R.id.txt_today_humidity)
    TextView mTxtTodayHumidity;
    @InjectView(R.id.txt_today_precipitations)
    TextView mTxtTodayPrecipitations;
    @InjectView(R.id.txt_today_pressure)
    TextView mTxtTodayPressure;
    @InjectView(R.id.txt_today_wind_speed)
    TextView mTxtTodayWindSpeed;
    @InjectView(R.id.txt_today_wind_speed_unit_symbol)
    TextView mTxtTodayWindSpeedUnitSymbol;
    @InjectView(R.id.txt_today_wind_direction)
    TextView mTxtTodayWindDirection;

    int mTodayStatsHeight;
    String temperatureUnit;
    String lenghtUnit;

    TodayWeatherWrapper mTodayWeatherWrapper;

    HashMap<String, Integer> mWeatherConditionBackgroundsMap;

    public static TodayFragment newInstance() {
        TodayFragment fragment = new TodayFragment();
        Bundle args = new Bundle();
        // put here your arguments (ex: args.putString(PARAM1_KEY, param1);)
        fragment.setArguments(args);
        return fragment;
    }


    public TodayFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // get here your arguments
        }
        if(activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setTitle(R.string.title_today);
        }

        mWeatherConditionBackgroundsMap = new HashMap<>();

        String[] weatherConditionCodes = getActivity().getResources()
                .getStringArray(R.array.weather_condition_codes);
        TypedArray weatherConditionBackgrounds = getActivity().getResources()
                .obtainTypedArray(R.array.weather_condition_backgrounds);

        for (int i = 0; i < weatherConditionCodes.length; i++) {
            mWeatherConditionBackgroundsMap.put(
                    weatherConditionCodes[i],weatherConditionBackgrounds.getResourceId(i,-1));
        }
        weatherConditionBackgrounds.recycle();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Timber.e("onCreateView ");

        View view = inflater.inflate(R.layout.fragment_today, container, false);
        ButterKnife.inject(this, view);

        mRelLayoutTodayStats.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mTodayStatsHeight = mRelLayoutTodayStats.getHeight();
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            mRelLayoutTodayStats.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            mRelLayoutTodayStats.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                        mRelLayoutTodayStats.setTranslationY(mTodayStatsHeight);
                    }
                });

        if (getLastLocation() != null) {
            Timber.e("last_location not null");
            renderTodayWeather();
        } else {
            Timber.e("last_location null");
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        readPref();
        if (getLastLocation() != null) {
            renderTodayWeather();
        }
    }

    @Override
    public void onLocationChanged() {
        super.onLocationChanged();
        loadTodayWeather();
    }


    public void readPref() {
        // get shared preferences
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activity);
        temperatureUnit = pref.getString(getString(R.string.weather_pref_temperature_key),
                        getString(R.string.weather_pref_temperature_units_default_value));
        lenghtUnit = pref.getString(getString(R.string.weather_pref_lenght_key),
                        getString(R.string.weather_pref_lenght_units_default_value));
    }


    public void loadTodayWeather() {
        hideUI();
        ApiClient.getRestInterface()
                .getTodayWeather(getLastLocation().latitude, getLastLocation().longitude,
                        new Callback<TodayWeatherWrapper>() {
                            @Override
                            public void success(TodayWeatherWrapper todayWeatherWrapper, Response response) {
                                Timber.e("API call successful.");
                                mTodayWeatherWrapper = todayWeatherWrapper;
                                renderTodayWeather();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Timber.e("API call failed.");
                                if (error.getResponse() != null) {
                                    Timber.e("Status: " + error.getResponse().getStatus());
                                    Timber.e("Reason: " + error.getResponse().getReason());
                                } else {
                                    Timber.e("Null response ");
                                }
                                activity.showNetworkErrorFragment();
                            }
                        });
    }


    public void showUI() {
        mProgressLoading.setVisibility(View.INVISIBLE);
        showTodaySummary();
        slideUpTodayStats();
    }


    public void hideUI() {
        mProgressLoading.setVisibility(View.VISIBLE);
        mRelLayoutTodaySummary.setAlpha(0);
        mRelLayoutTodayStats.setTranslationY(mTodayStatsHeight);
    }


    public void renderTodayWeather() {
        showUI();

        // setting city name
        mTxtTodayCityName.setText(mTodayWeatherWrapper.getName());

        // weather conditions
        if (mTodayWeatherWrapper.getWeather() != null) {
            //setting weather description
            String conditions = mTodayWeatherWrapper.getWeather().get(0).getDescription();
            mTxtTodayWeatherCondition.setText(conditions);

            // getting the weather icon code
            String weatherConditionCode = mTodayWeatherWrapper.getWeather().get(0).getIcon();
            weatherConditionCode = weatherConditionCode
                    .substring(0,weatherConditionCode.length()-1);
            Timber.d("weather code: " + weatherConditionCode);

            // setting the background depending on the weather icon code
            Glide.with(activity)
                    .load(mWeatherConditionBackgroundsMap.get(weatherConditionCode))
                    .error(R.drawable.sf)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mImgTodayCityPic);


            // loading the weather condition icon
            String iconUrl = WeatherConfig.OPENWEATHER_ICONS_URL +
                    mTodayWeatherWrapper.getWeather().get(0).getIcon() + "." +
                    WeatherConfig.OPENWEATHER_ICONS_EXT;
            Glide.with(activity)
                    .load(iconUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mImgTodayWeatherCondition);
        }

        // main info
        if (mTodayWeatherWrapper.getMain() != null) {
            // setting temperature
            mTxtTodayTemperature.setText(String.valueOf(
                    mTodayWeatherWrapper.getMain().getTemp(activity, temperatureUnit)));

            // setting humidity
            mTxtTodayHumidity.setText(String.valueOf(mTodayWeatherWrapper.getMain().getHumidity()));

            // setting pressure
            mTxtTodayPressure.setText(String.valueOf(mTodayWeatherWrapper.getMain().getPressure()));
        }

        // rain info - the current version of OpenWeather API doesn't have this field
        if (mTodayWeatherWrapper.getRain() != null) {
            // setting precipitations
            mTxtTodayPrecipitations.setText(
                    String.valueOf(mTodayWeatherWrapper.getRain().getPrecipitation()));
        }

        // wind info
        if (mTodayWeatherWrapper.getWind() != null) {


            // setting wind speed
            mTxtTodayWindSpeed.setText(String.valueOf(
                    mTodayWeatherWrapper.getWind().getSpeed(activity, lenghtUnit)));

            // setting wind speed unit symbol
            mTxtTodayWindSpeedUnitSymbol.setText(
                    UnitsUtils.getWindSpeedUnitSymbol(activity, lenghtUnit));

            // setting wind direction
            String dir = UnitsUtils.deg2direction(mTodayWeatherWrapper.getWind().getDeg());
            mTxtTodayWindDirection.setText(dir);
        }

        mProgressLoading.setVisibility(View.GONE);
    }


    public void showTodaySummary() {
        mRelLayoutTodaySummary.animate().cancel();
        mRelLayoutTodaySummary.setAlpha(0);
        mRelLayoutTodaySummary.animate().alpha(1).setDuration(200).start();
    }


    public void slideUpTodayStats() {
        mRelLayoutTodayStats.animate().cancel();
        mRelLayoutTodayStats.setTranslationY(mTodayStatsHeight);
        mRelLayoutTodayStats.animate().translationY(0).start();
    }
}
