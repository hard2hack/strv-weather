package org.dolen.weather.android.api.model;

import java.io.Serializable;
import java.util.ArrayList;

public class TodayWeatherWrapper implements Serializable {
    private int dt;
    private ArrayList<WeatherSummary> weather;
    private WeatherMainInfo main;
    private WeatherWindInfo wind;
    private WeatherRainInfo rain;
    private int id;
    private String name;
    private String dt_txt;


    public int getDt() {
        return dt;
    }


    public void setDt(int dt) {
        this.dt = dt;
    }


    public ArrayList<WeatherSummary> getWeather() {
        return weather;
    }


    public void setWeather(ArrayList<WeatherSummary> weather) {
        this.weather = weather;
    }


    public WeatherMainInfo getMain() {
        return main;
    }


    public void setMain(WeatherMainInfo main) {
        this.main = main;
    }


    public WeatherWindInfo getWind() {
        return wind;
    }


    public void setWind(WeatherWindInfo wind) {
        this.wind = wind;
    }


    public WeatherRainInfo getRain() {
        return rain;
    }


    public void setRain(WeatherRainInfo rain) {
        this.rain = rain;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDt_txt() {
        return dt_txt;
    }


    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
