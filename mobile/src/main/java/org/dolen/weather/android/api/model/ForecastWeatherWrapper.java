package org.dolen.weather.android.api.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ForecastWeatherWrapper implements Serializable {
    private ArrayList<TodayWeatherWrapper> list;

    public ArrayList<TodayWeatherWrapper> getList() {
        return list;
    }


    public void setList(ArrayList<TodayWeatherWrapper> list) {
        this.list = list;
    }
}
